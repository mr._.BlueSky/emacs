;; .emacs

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(column-number-mode t)
 '(custom-enabled-themes (quote (tango-dark)))
 '(custom-safe-themes
   (quote
    ("0cd56f8cd78d12fc6ead32915e1c4963ba2039890700458c13e12038ec40f6f5" "a24c5b3c12d147da6cef80938dca1223b7c7f70f2f382b26308eba014dc4833a" "c616e584f7268aa3b63d08045a912b50863a34e7ea83e35fcab8537b75741956" default)))
 '(diff-switches "-u")
 '(display-time-mode t)
 '(fringe-mode (quote (nil . 0)) nil (fringe))
 '(indicate-empty-lines t)
 '(package-selected-packages
   (quote
    (rust-mode bash-completion py-autopep8 material-theme indent-guide flycheck elpy ein better-defaults airline-themes)))
 '(python-shell-interpreter "/usr/bin/python3")
 '(save-place-mode t)
 '(show-paren-mode t)
 '(size-indication-mode t)
 '(tool-bar-mode nil))

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)
;;https://realpython.com/emacs-the-best-python-editor/
(require 'package)
(setq package-archives '(("gnu" . "https://elpa.gnu.org/packages/")
                         ("marmalade" . "https://marmalade-repo.org/packages/")
                         ("melpa" . "https://melpa.org/packages/")))

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(defvar myPackages
  '(better-defaults
    company
    ein
    elpy
    fill-column-indicator
    flycheck
    material-theme
    auto-sudoedit
    bash-completion
    airline-themes
    indent-guide
    bash-completion
    racer
    rust-auto-use
    rust-mode
    cargo
    flymake-rust
    racer
    py-autopep8))
(mapc #'(lambda (package)
    (unless (package-installed-p package)
      (package-install package)))
      myPackages)

(elpy-enable)

(when (require 'flycheck nil t)
  (setq elpy-modules (delq 'elpy-module-flymake elpy-modules))
  (add-hook 'elpy-mode-hook 'flycheck-mode))

;; enable autopep8 formatting on save
(require 'py-autopep8)
(add-hook 'elpy-mode-hook 'py-autopep8-enable-on-save)

;; keep a log of window states so that you can undo/redo with C-c <left>/<right>
(when (fboundp 'winner-mode)
  (winner-mode 1))

;; line numbers
(global-linum-mode t)
;; highlight matching
(show-paren-mode t)
;; electric-pairs
(electric-pair-mode t)
;; font sttings
(set-frame-font "Liberation Mono 12" nil t)


;; restore previous session
(desktop-save-mode 1)

;; airline theme
(require 'airline-themes)
(load-theme 'airline-kolor)

;; indent-guide
(indent-guide-global-mode)

;; auto-sudoedit
(auto-sudoedit-mode 1)

;; flymake-rust
(require 'flymake-rust)
(add-hook 'rust-mode-hook 'flymake-rust-load)

;; racer for rust
(add-hook 'rust-mode-hook #'racer-mode)

(add-hook 'racer-mode-hook #'company-mode)

(require 'rust-mode)
(define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
(setq company-tooltip-align-annotations t)

;; visual line mode
(visual-line-mode t)

;; icomplete mode
(icomplete-mode t)

;; fill column indicator
(setq fci-rule-column 80)
(setq fci-rule-width 1)
(setq fci-rule-color "orange")
(define-globalized-minor-mode global-fci-mode fci-mode (lambda () (fci-mode 1)))
(global-fci-mode 1)

;; check spelling in the buffer
(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-prog-mode 1))))
